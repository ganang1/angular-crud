import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs';
import { Router } from "@angular/router";


@Injectable()
export class AuthService {

  get windowRef() {
    return window
  }

  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, public router: Router, ) {
    this.user = firebaseAuth.authState;
  }

  // Reset Forggot password
  forgotpassword(email: string) {
    this.firebaseAuth.auth.sendPasswordResetEmail(email)
      .then(() => {
        window.alert('Link Reset Password telah dikirim, Silahkan cek inbox anda!');
      }).catch((error) => {
        window.alert('Silahkan masukkan alamat Email yang telah didaftarkan!')
      })
  }

  forgotemail(email: string) {
    window.alert('Silahkan email ke : svrizkyp007@gmail.com')
  }

  // signup(email: string, password: string) {
  //   this.firebaseAuth
  //     .auth
  //     .createUserWithEmailAndPassword(email, password)
  //     .then(value => {
  //       console.log('Success!', value);
  //     })
  //     .catch(err => {
  //       console.log('Something went wrong:',err.message);
  //     });    
  // }

  login(email: string, password: string) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        window.alert('Berhasil Login!!')
        this.router.navigate(['menu']);
      })
      .catch(err => {
        window.alert('Email atau Password Salah!')
      });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut()
      .then(value => {
        window.alert('Berhasil Logout!')
        this.router.navigate(['login']);
      })
  }

}
