import { Component,OnInit } from '@angular/core';
import { CrudService } from './crud.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'SVR';
  students: any;
  studentName: string;
  studentAge: number;
  studentGender: string;
  studentAddress: string;
  studentPhone: number;

  constructor(private crudService: CrudService, public route: ActivatedRoute) { }
 
  ngOnInit() {
    this.crudService.read_Tiket().subscribe(data => {
 
      this.students = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          Name: e.payload.doc.data()['Name'],
          Age: e.payload.doc.data()['Age'],
          Gender: e.payload.doc.data()['Gender'],
          Address: e.payload.doc.data()['Address'],
          Phone: e.payload.doc.data()['Phone'],
        };
      })
      console.log(this.students);
 
    });
  }
 
  CreateRecord() {
    let record = {};
    record['Name'] = this.studentName;
    record['Age'] = this.studentAge;
    record['Gender'] = this.studentGender;
    record['Address'] = this.studentAddress;
    record['Phone'] = this.studentPhone;
    this.crudService.create_NewTiket(record).then(resp => {
      this.studentName = "";
      this.studentAge = undefined;
      this.studentAddress = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }
 
  RemoveRecord(rowID) {
    this.crudService.delete_Tiket(rowID);
  }
 
  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.Name;
    record.EditAge = record.Age;
    record.EditGender = record.Gender;
    record.EditAddress = record.Address;
    record.EditPhone = record.Phone;
  }
 
  UpdateRecord(recordRow) {
    let record = {};
    record['Name'] = recordRow.EditName;
    record['Age'] = recordRow.EditAge;
    record['Gender'] = recordRow.EditGender;
    record['Address'] = recordRow.EditAddress;
    record['Phone'] = recordRow.EditPhone;
    this.crudService.update_Tiket(recordRow.id, record);
    recordRow.isEdit = false;
  }
}
