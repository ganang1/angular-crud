import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  title = 'SVR';
  students: any;
  studentName: string;
  studentAge: number;
  studentGender: string;
  studentAddress: string;
  studentPhone: number;
  studentStatus: string;

  constructor(public authService: AuthService, private crudService: CrudService) { }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {
    this.crudService.read_Tiket().subscribe(data => {
 
      this.students = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          Name: e.payload.doc.data()['Name'],
          Age: e.payload.doc.data()['Age'],
          Gender: e.payload.doc.data()['Gender'],
          Address: e.payload.doc.data()['Address'],
          Phone: e.payload.doc.data()['Phone'],
          Status: e.payload.doc.data()['Status'],
        };
      })
      console.log(this.students);
 
    });
  }

  CreateRecord() {
    let record = {};
    record['Name'] = this.studentName;
    record['Age'] = this.studentAge;
    record['Gender'] = this.studentGender;
    record['Address'] = this.studentAddress;
    record['Phone'] = this.studentPhone;
    record['Status'] = this.studentStatus;
    this.crudService.create_NewTiket(record).then(resp => {
      this.studentName = "";
      this.studentAge = undefined;
      this.studentGender = undefined;
      this.studentAddress = "";
      this.studentPhone = undefined;
      this.studentAddress = "";
      this.studentStatus = undefined;
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }
 
  RemoveRecord(rowID) {
    this.crudService.delete_Tiket(rowID);
  }
 
  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.Name;
    record.EditAge = record.Age;
    record.EditGender = record.Gender;
    record.EditAddress = record.Address;
    record.EditPhone = record.Phone;
    record.EditStatus = record.Status;
  }
 
  UpdateRecord(recordRow) {
    let record = {};
    record['Name'] = recordRow.EditName;
    record['Age'] = recordRow.EditAge;
    record['Gender'] = recordRow.EditGender;
    record['Address'] = recordRow.EditAddress;
    record['Phone'] = recordRow.EditPhone;
    record['Status'] = recordRow.EditStatus;
    this.crudService.update_Tiket(recordRow.id, record);
    recordRow.isEdit = false;
  }

}
