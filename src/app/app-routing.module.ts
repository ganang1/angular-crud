import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { PelangganComponent } from './pelanggan/pelanggan.component';
import { PegawaiComponent } from './pegawai/pegawai.component';
import { ProdukComponent } from './produk/produk.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'pelanggan', component: PelangganComponent },
  { path: 'pegawai', component: PegawaiComponent },
  { path: 'produk', component: ProdukComponent },
  {
    path: '',
    redirectTo: '/menu',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
