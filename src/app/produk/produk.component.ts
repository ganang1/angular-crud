import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';


@Component({
  selector: 'app-produk',
  templateUrl: './produk.component.html',
  styleUrls: ['./produk.component.css']
})
export class ProdukComponent implements OnInit {

  produk: any;
  nama: string;
  harga: number;
  jenis: string;
  formTambah: boolean;
  formEdit: boolean;

  constructor(public authService: AuthService, private crudService: CrudService) { }

  ngOnInit() {
    this.formTambah = false;
    this.formEdit = false;
    this.crudService.read_Produk().subscribe(data => {

      this.produk = data.map(e => {
        return {
          id: e.payload.doc.id,
          nama: e.payload.doc.data()['nama'],
          harga: e.payload.doc.data()['harga'],
          jenis: e.payload.doc.data()['jenis'],
        };
      })
      console.log(this.produk);

    });
  }

  changeForm() {
    this.formTambah = true;
    this.formEdit = false;
  }

  CreateRecord() {
    let record = {};
    record['nama'] = this.nama;
    record['harga'] = this.harga;
    record['jenis'] = this.jenis;
    this.crudService.create_NewProduk(record).then(resp => {
      this.nama = "";
      this.harga = undefined;
      this.jenis = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
    this.formTambah = false;
  }

  RemoveRecord(rowID) {
    this.crudService.delete_Produk(rowID);
  }

  EditRecord(record) {
    this.formEdit = true;
    this.formTambah = false;
    record.EditNama = record.nama;
    record.EditJenis = record.jenis;
    record.EditHarga = record.harga;
  }

  UpdateRecord(recordRow) {
    let record = {};
    record['nama'] = recordRow.EditNama;
    record['harga'] = recordRow.EditHarga;
    record['jenis'] = recordRow.EditJenis;
    this.crudService.update_Produk(recordRow.id, record);
    this.formEdit = false;
    this.formTambah = false;
  }

}
