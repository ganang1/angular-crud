import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-pegawai',
  templateUrl: './pegawai.component.html',
  styleUrls: ['./pegawai.component.css']
})
export class PegawaiComponent implements OnInit {

  tiket: any;
  tipe_kereta: string;
  nama_tiket: string;
  jenis_tiket: string;
  stasiun_awal: string;
  stasiun_tujuan: Date;
  tanggal_keberangkatan: Date;
  harga: number;
  formTambah: boolean;
  formEdit: boolean;

  constructor(public authService: AuthService, private crudService: CrudService) { }

  ngOnInit() {
    this.formTambah = false;
    this.formEdit = false;
    this.crudService.read_Tiket().subscribe(data => {

      this.tiket = data.map(e => {
        return {
          id: e.payload.doc.id,

          tipe_kereta: e.payload.doc.data()['tipe_kereta'],
          nama_tiket: e.payload.doc.data()['nama_tiket'],
          jenis_tiket: e.payload.doc.data()['jenis_tiket'],
          stasiun_awal: e.payload.doc.data()['stasiun_awal'],
          stasiun_tujuan: e.payload.doc.data()['stasiun_tujuan'],
          tanggal_keberangkatan: e.payload.doc.data()['tanggal_keberangkatan'],
          harga: e.payload.doc.data()['harga'],
         
        };
      })
      console.log(this.tiket);

    });
  }

  changeForm() {
    this.formTambah = true;
    this.formEdit = false;
  }

  // CreateRecord() {
  //   let record = {};
  //   record['tipe_kereta'] = this.tipe_kereta;
  //   record['nama_kereta'] = this.nama_kereta;
  //   record['stasiun_awal'] = this.stasiun_awal;
  //   record['stasiun_tujuan'] = this.stasiun_tujuan;
  //   record['tanggal_keberangkatan'] = this.tanggal_keberangkatan;
    
  //   this.crudService.create_NewTiket(record).then(resp => {
  //     this.tipe_kereta = "";
  //     this.nama_kereta = "";
  //     this.stasiun_awal = "";
  //     this.stasiun_tujuan = "";
  //     this.tanggal_keberangkatan = "";
      
  //     console.log(resp);
  //   })
  //     .catch(error => {
  //       console.log(error);
  //     });
  //   this.formTambah = false;
  // }

  RemoveRecord(rowID) {
    this.crudService.delete_Tiket(rowID);
  }

  EditRecord(record) {
    this.formEdit = true;
    this.formTambah = false;
    record.EditTipe_kereta = record.tipe_kereta;
    record.EditNama_tiket = record.nama_tiket;
    record.EditJenis_tiket = record.jenis_tiket;
    record.EditStasiun_awal = record.stasiun_awal;
    record.EditStasiun_tujuan= record.stasiun_tujuan;
    record.EditTanggal_keberangkatan = record.tanggal_keberangkatan;
    record.EditHarga = record.harga;
   
  }

  UpdateRecord(recordRow) {
    let record = {};
    record['tipe_kereta'] = recordRow.EditTipe_kereta;
    record['nama_tiket'] = recordRow.EditNama_tiket;
    record['jenis_tiket'] = recordRow.EditJenis_tiket;
    record['stasiun_awal'] = recordRow.EditStasiun_awal;
    record['stasiun_tujuan'] = recordRow.EditStasiun_tujuan;
    record['tanggal_keberangkatan'] = recordRow.EditTanggal_keberangkatan;
    record['harga'] = recordRow.EditHarga;
    this.crudService.update_Tiket(recordRow.id, record);
    this.formEdit = false;
    this.formTambah = false;
  }

}
