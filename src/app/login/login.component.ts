import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from "@angular/router";

import * as firebase from 'firebase/app';

export class PhoneNumber {
  phone: string;

  get phnumber() {
    const num = this.phone
    return `+${num}`
  }
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  windowRef: any;
  phoneNumber = new PhoneNumber()
  verificationCode: string;
  user: any;

  email: string;
  password: string;

  constructor(public authService: AuthService, public router: Router) { }

  // signup() {
  //   this.authService.signup(this.email, this.password);
  //   this.email = this.password = '';
  // }

  login() {
    this.authService.login(this.email, this.password);
    this.email = this.password = '';
  }

  forgot() {
    this.authService.forgotpassword(this.email);
    this.email = this.password = '';
  }

  forgotemail() {
    this.authService.forgotemail(this.email);
    this.email = this.password = '';
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {
    this.windowRef = this.authService.windowRef
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')

    this.windowRef.recaptchaVerifier.render()
  }
  sendLoginCode() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    const num = this.phoneNumber.phnumber;

    firebase.auth().signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.windowRef.confirmationResult = result;
      })
      .catch(error => window.alert('Nomor HP tidak valid'));
  }

  verifyLoginCode() {
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then(result => {
        window.alert('Berhasil Login!!')
        this.router.navigate(['menu']);
      })
      .catch(error => window.alert('Silahkan Masukkan Kode Verifikasi Yang Valid!'))
  }

}
