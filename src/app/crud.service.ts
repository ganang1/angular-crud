import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(
    private firestore: AngularFirestore
  ) { }


  create_NewTiket(record) {
    return this.firestore.collection('tiket').add(record);
  }

  create_NewPenumpang(record) {
    return this.firestore.collection('penumpang').add(record);
  }
  create_NewPemesanan(record) {
    return this.firestore.collection('pemesanan').add(record);
  }
  create_NewProduk(record) {
    return this.firestore.collection('produk').add(record);
  }

  read_Tiket() {
    return this.firestore.collection('tiket').snapshotChanges();
  }
  read_Penumpang() {
    return this.firestore.collection('penumpang').snapshotChanges();
  }
  read_Pemesanan() {
    return this.firestore.collection('pemesanan').snapshotChanges();
  }
  read_Produk() {
    return this.firestore.collection('produk').snapshotChanges();
  }

  update_Tiket(recordID, record) {
    this.firestore.doc('tiket/' + recordID).update(record);
  }
  update_Penumpang(recordID, record) {
    this.firestore.doc('penumpang/' + recordID).update(record);
  }
  update_Pemesanan(recordID, record) {
    this.firestore.doc('pemesanan/' + recordID).update(record);
  }
  update_Produk(recordID, record) {
    this.firestore.doc('produk/' + recordID).update(record);
  }

  delete_Tiket(record_id) {
    this.firestore.doc('tiket/' + record_id).delete();
  }
  delete_Penumpang(record_id) {
    this.firestore.doc('penumpang/' + record_id).delete();
  }
  delete_Pemesanan(record_id) {
    this.firestore.doc('pemesanan/' + record_id).delete();
  }
  delete_Produk(record_id) {
    this.firestore.doc('produk/' + record_id).delete();
  }
}
