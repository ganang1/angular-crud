import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.component.html',
  styleUrls: ['./pelanggan.component.css']
})
export class PelangganComponent implements OnInit {
  pemesanan: any;
  nama_penumpang: string;
  alamat: string;
  no_hp: number;
  stasiun_awal: string;
  stasiun_tujuan: string;
  tgl_keberangkatan: Date;
  formTambah: boolean;
  formEdit: boolean;

  constructor(public authService: AuthService, private crudService: CrudService) { }

  ngOnInit() {
    this.formTambah = false;
    this.formEdit = false;
    this.crudService.read_Pemesanan().subscribe(data => {

      this.pemesanan = data.map(e => {
        return {
          id: e.payload.doc.id,
          nama_penumpang: e.payload.doc.data()['nama_penumpang'],
          alamat: e.payload.doc.data()['alamat'],
          no_hp: e.payload.doc.data()['no_hp'],
          stasiun_awal: e.payload.doc.data()['stasiun_awal'],
          stasiun_tujuan: e.payload.doc.data()['stasiun_tujuan'],
          tgl_keberangkatan: e.payload.doc.data()['tgl_keberangkatan'],
        };
      })
      console.log(this.pemesanan);

    });
  }

  changeForm() {
    this.formTambah = true;
    this.formEdit = false;
  }

  CreateRecord() {
    let record = {};
    record['nama_penumpang'] = this.nama_penumpang;
    record['alamat'] = this.alamat;
    record['no_hp'] = this.no_hp;
    record['stasiun_awal'] = this.stasiun_awal;
    record['stasiun_tujuan'] = this.stasiun_tujuan;
    record['tgl_keberangkatan'] = this.tgl_keberangkatan;
    this.crudService.create_NewPemesanan(record).then(resp => {
      this.nama_penumpang = "";
      this.alamat = "";
      this.no_hp = undefined;
      this.stasiun_awal = "";
      this.stasiun_tujuan = "";
      this.tgl_keberangkatan = undefined;
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
    this.formTambah = false;
  }

  RemoveRecord(rowID) {
    this.crudService.delete_Pemesanan(rowID);
  }

  EditRecord(record) {
    this.formEdit = true;
    this.formTambah = false;
    record.EditNama = record.nama_penumpang;
    record.EditAlamat = record.alamat;
    record.EditNo_hp = record.no_hp;
    record.EditStasiun_awal = record.stasiun_awal;
    record.EditStasiun_tujuan = record.stasiun_tujuan;
    record.EditTgl_keberangkatan = record.tgl_keberangkatan;
  }

  UpdateRecord(recordRow) {
    let record = {};
    record['nama_penumpang'] = recordRow.EditNama;
    record['alamat'] = recordRow.EditAlamat;
    record['no_hp'] = recordRow.EditNo_hp;
    record['stasiun_awal'] = recordRow.EditStasiun_awal;
    record['stasiun_tujuan'] = recordRow.EditStasiun_tujuan;
    record['tgl_keberangkatan'] = recordRow.EditTgl_keberangkatan;
    this.crudService.update_Pemesanan(recordRow.id, record);
    this.formEdit = false;
    this.formTambah = false;
  }

}
